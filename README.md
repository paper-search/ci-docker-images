# CI Docker Images
All docker images used by the CI.

## Manual build

### Install Docker

#### Fedora 33
Based on: https://docs.docker.com/engine/install/fedora/#installation-methods
```bash
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf -y install docker-ce docker-ce-cli containerd.io
sudo reboot -h now
```

### PS Linux Build Image

Change into the correct directory where the `Dockerfile` exists.
```bash
cd ps_linux_build
```

Start the docker service and login.
```bash
sudo systemctl start docker
sudo docker login
```

Build the image.
```bash
sudo docker image build .
```

Tag the build image.
**Replace** `<TAG>` with the build result tag (Successfully build `<TAG>`).
```bash
sudo docker tag <TAG> cockrain/ps_linux_build:latest
```

Upload the build and tagged image.
```bash
sudo docker push cockrain/ps_linux_build:latest
```

### PS Windows Build Image

Change into the correct directory where the `Dockerfile` exists.
```bash
cd ps_windows_build
```

Start the docker service and login.
```bash
sudo systemctl start docker
sudo docker login
```

Build the image.
```bash
sudo docker image build .
```

Tag the build image.
**Replace** `<TAG>` with the build result tag (Successfully build `<TAG>`).
```bash
sudo docker tag <TAG> cockrain/ps_windows_build:latest
```

Upload the build and tagged image.
```bash
sudo docker push cockrain/ps_windows_build:latest
```
